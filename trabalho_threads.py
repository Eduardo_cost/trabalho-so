import _thread
import time

class Dados(object): #(POO)
    def __init__(self, nm, vl=1000):
        self.nome = nm
        self.valores = vl

def recebe_threads(valor_transf, recebendo_bloqueio):   #local onde recebe as threads
    recebendo_bloqueio.acquire()
    if pessoa_1.valores >= valor_transf:   #verifica se a pessoa tem o saldo suficiente
        pessoa_1.valores -= valor_transf     #retira o valor da pessoa um e deposita na pessoa 2 abaixo
        pessoa_2.valores += valor_transf      #deposito
        print("Conta_1:", pessoa_1.nome, "___; Saldo:", pessoa_1.valores)
        print("Conta_2", pessoa_2.nome, "___; Saldo:", pessoa_2.valores)
        print('-=-=-=' * 20)
        print('\n')
        recebendo_bloqueio.release()

if __name__ == '__main__':
    pessoa_1 = Dados("Eduardo")  #pessoas
    pessoa_2 = Dados("Pedro")    #
    print("Conta_1:", pessoa_1.nome, "___; Saldo:", pessoa_1.valores)
    print("Conta_2:", pessoa_2.nome, "___; Saldo:", pessoa_2.valores)
    print('---')

    #criando threads
    bloqueio  = _thread.allocate_lock()
    for i in range(0, 100):
        _thread.start_new_thread(recebe_threads, (1, bloqueio)) #essa funçao cria threads e obrigatoriamente pede uma funçao e uma lista de argumento(tupla), a quantidade de argumento varia de acordo com a quantidade de argumentos esta pedindo la em cima
    time.sleep(1)


 

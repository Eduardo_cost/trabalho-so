# Trabalho SO

- Nome: Eduardo da costa; RA: 21905136
- Nome: Pedro Menezes; RA: 21910286


# Execuções:

# LINUX:

Utilizamos alguns comandos apresentado pelo professor ,como;
- MKDIR (usado para criar uma pasta), exemplo; mkdir exemplo
- cd (usado para entrar na pasta criada), exemplo; cd exemplo
- e ls (para ver o que tem na pasta), exemplo; ls

dentro do terminal:
- copia o clone do site do git ( git@gitlab.com:Eduardo_cost/trabalho-so.git)
- e execute o comando "python3 threads.py" (para executar o programa na linguagem python)

# "Windows"

dentro do terminal;

- git@gitlab.com:Eduardo_cost/trabalho-so.git
- encontra o executavel e depois o script (com a extensão ".py").
- usa esse endereço do python no terminal entre aspas duplas e o arquivo(.py) 
- Depois, aperte a tecla (enter), e ira rodar.